﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// Jeremy Gillespie-Cloutier (260688666)
// This class handles the game user interface
public class Menu : MonoBehaviour
{

    private Text textSuccess, textFailure; // Text to display if the agent suceeds or fails
    private Button buttonRestart; // button to restart the game
    private Toggle toggleFollow; // Toggle indicating if the camera should follow the agent
    private Game game;

    void Start()
    {
        game = GameObject.Find("Level").GetComponent<Game>();
        textSuccess = GameObject.Find("TextSuccess").GetComponent<Text>();
        textFailure = GameObject.Find("TextFailure").GetComponent<Text>();
        buttonRestart = GameObject.Find("ButtonRestart").GetComponent<Button>();
        toggleFollow = GameObject.Find("ToggleFollow").GetComponent<Toggle>();
        // When the follow toggle is selected, tell the game that the camera should follow the agent
        toggleFollow.onValueChanged.AddListener((selected) =>
        {
            game.followAgent = selected;
        });
        // Reload the scene when the restart button is pressed
        buttonRestart.onClick.AddListener(() =>
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        });
    }

    public void showResult(bool result)
    {
        // Display the result of the game (agent succeded or failed)
        if (result)
            textSuccess.enabled = true;
        else
            textFailure.enabled = true;
    }
}

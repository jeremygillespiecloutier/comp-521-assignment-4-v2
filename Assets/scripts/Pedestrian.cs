﻿using Assets.scripts.grammar;
using UnityEngine;
using UnityEngine.AI;

// Jeremy Gillespie-Cloutier (260688666)
// Class containing information about pedestrians
public class Pedestrian : MonoBehaviour
{

    public const float SIZE = 1.5f;
    public const float SPEED = Agent.SPEED * 1.5f; // Pedestrians go 1.5x faster than the agent
    public const float LOOK_SPEED = 180f; // The speed at which the pedestrian looks around (rotation speed) degrees per second
    private Grammar grammar; // The pedestrian's grammar
    public bool completed { get { return grammar.completed; } }
    [System.NonSerialized] public Animator animator;
    [System.NonSerialized] public NavMeshAgent navAgent;
    public int priority; // The priority of the pedestrian's nav mesh agent
    Game game;

    void Start()
    {
        transform.localScale = new Vector3(1, 1, 1) * SIZE;
        animator = GetComponent<Animator>();
        navAgent = GetComponent<NavMeshAgent>();
        navAgent.speed = Pedestrian.SPEED;
        navAgent.avoidancePriority = priority;
        game = GameObject.Find("Level").GetComponent<Game>();
        // Create the grammar for the pedestrian
        grammar = new Grammar(this);
    }

    void Update()
    {
        // Execute the grammar actions
        if (game.running)
            grammar.update(Time.deltaTime);
    }

}

﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

// Jeremy Gillespie-Cloutier (260688666)
// Generates the level
public class Level : MonoBehaviour
{

    public const float P_WIDTH = 130; // Width of the central platform
    public const float P_HEIGHT = NUM_LANES * LANE_HEIGHT + (NUM_LANES - 1) * SEPERATOR_HEIGHT; // Height of the central platform
    public const float EDGE_WIDTH = 5f; // Wdidth of the orange (entrance) and green (exit) areas
    public const float ROOM_WIDTH = 8f; // width of agent spwan rooms
    public const float ROOM_HEIGHT = 15f; // height of rooms
    public const float ROOM_DISTANCE = 2f / 3 * P_HEIGHT + ROOM_WIDTH; // distance between rooms
    const int NUM_SEPERATORS = 40; // number of lane seperators (horizontally)
    const float SEPERATOR_WIDTH = 2f; // width of each seperator
    const float SEPERATOR_HEIGHT = 1f; // height of each seperator
    const int NUM_LANES = 4; // number of lanes
    public const float START_X = -(P_WIDTH + EDGE_WIDTH) / 2;
    public const float STOP_X = -START_X;
    public const float LANE_HEIGHT = 2.5f * Agent.SIZE; // height of each lane
    [System.NonSerialized] public List<Vector3> agentSpawnPoints = new List<Vector3>(); // all agent spawn locations
    [System.NonSerialized] public List<Vector3> pedestrianSpawnPoints = new List<Vector3>(); // all pedestrian spawn locations
    GameObject navigationArea; // Area in which a navigation mesh will be built

    void Awake()
    {
        generate();
    }

    void generate()
    {
        navigationArea = GameObject.Find("NavigationArea");
        // Create the central platform
        GameObject platform = GameObject.CreatePrimitive(PrimitiveType.Cube);
        platform.transform.position = Vector3.zero;
        platform.transform.localScale = new Vector3(P_WIDTH, 1, P_HEIGHT);
        platform.transform.parent = navigationArea.transform;
        GameObject start = GameObject.CreatePrimitive(PrimitiveType.Cube);
        // Create the starting area
        Material orange = (Material)Resources.Load("materials/Orange");
        start.GetComponent<Renderer>().material = orange;
        start.transform.position = new Vector3(-(P_WIDTH + EDGE_WIDTH) / 2f, 0, 0);
        start.transform.localScale = new Vector3(EDGE_WIDTH, 1, P_HEIGHT);
        start.transform.parent = navigationArea.transform;
        // Create the exit area
        GameObject end = GameObject.CreatePrimitive(PrimitiveType.Cube);
        Material green = (Material)Resources.Load("materials/Green");
        end.GetComponent<Renderer>().material = green;
        end.transform.position = new Vector3((P_WIDTH + EDGE_WIDTH) / 2f, 0, 0);
        end.transform.localScale = new Vector3(EDGE_WIDTH, 1, P_HEIGHT);
        end.transform.parent = navigationArea.transform;
        // Create the rooms
        float startPos = -(P_WIDTH + ROOM_WIDTH) / 2f;
        for (int i = 0; i < 4; i++)
        {
            startPos += ROOM_DISTANCE;
            GameObject upRoom = GameObject.CreatePrimitive(PrimitiveType.Cube);
            upRoom.transform.position = new Vector3(startPos, 0, (P_HEIGHT + ROOM_HEIGHT) / 2f);
            upRoom.transform.localScale = new Vector3(ROOM_WIDTH, 1, ROOM_HEIGHT);
            upRoom.transform.parent = navigationArea.transform;
            // Mark the area as a room on the navmesh so that pedestrians can't enter it
            GameObjectUtility.SetStaticEditorFlags(upRoom, StaticEditorFlags.NavigationStatic);
            GameObjectUtility.SetNavMeshArea(upRoom, 3);
            Vector3 spawn1 = new Vector3(startPos, 0.5f + Agent.SIZE / 2, P_HEIGHT / 2f + ROOM_HEIGHT - Agent.SIZE / 2f);
            agentSpawnPoints.Add(spawn1);

            GameObject downRoom = GameObject.CreatePrimitive(PrimitiveType.Cube);
            downRoom.transform.position = new Vector3(startPos, 0, -(P_HEIGHT + ROOM_HEIGHT) / 2f);
            downRoom.transform.localScale = new Vector3(ROOM_WIDTH, 1, ROOM_HEIGHT);
            downRoom.transform.parent = navigationArea.transform;
            GameObjectUtility.SetStaticEditorFlags(downRoom, StaticEditorFlags.NavigationStatic);
            GameObjectUtility.SetNavMeshArea(downRoom, 3);
            Vector3 spawn2 = new Vector3(startPos, 0.5f + Agent.SIZE / 2, -P_HEIGHT / 2f - ROOM_HEIGHT + Agent.SIZE / 2f);
            agentSpawnPoints.Add(spawn2);
        }
        // Create the lanes
        float spacingX = (P_WIDTH - NUM_SEPERATORS * SEPERATOR_WIDTH) / NUM_SEPERATORS;
        float startX = -(P_WIDTH - SEPERATOR_WIDTH) / 2f;

        for (int i = 0; i < NUM_SEPERATORS; i++)
        {
            float startZ = -(P_HEIGHT - SEPERATOR_HEIGHT) / 2f + LANE_HEIGHT;
            for (int j = 0; j < NUM_LANES - 1; j++)
            {
                // Create the line seperator
                GameObject seperator = GameObject.CreatePrimitive(PrimitiveType.Cube);
                seperator.transform.parent = navigationArea.transform;
                seperator.tag = "seperation";
                Material grey = (Material)Resources.Load("materials/Grey");
                seperator.GetComponent<Renderer>().material = grey;
                seperator.transform.position = new Vector3(startX, 0.05f, startZ);
                seperator.transform.localScale = new Vector3(SEPERATOR_WIDTH, 1, SEPERATOR_HEIGHT);
                startZ += LANE_HEIGHT + SEPERATOR_HEIGHT;
            }
            startX += spacingX + SEPERATOR_WIDTH;
        }

        // Find the pedestrian spawn locations
        float pedestrianZ = -(P_HEIGHT - LANE_HEIGHT) / 2f;
        float pedestrianX = -(P_WIDTH + EDGE_WIDTH) / 2f;
        for (int i = 0; i < NUM_LANES; i++)
        {
            Vector3 spawn = new Vector3(pedestrianX, 0.5f, pedestrianZ);
            pedestrianSpawnPoints.Add(spawn);
            pedestrianZ += LANE_HEIGHT + SEPERATOR_HEIGHT;
        }

        // Add the objects (except the lane seperators) of the level to the navigation mesh
        for (int i = 0; i < navigationArea.transform.childCount; i++)
        {
            GameObject obj = navigationArea.transform.GetChild(i).gameObject;
            if (obj.tag != "seperation")
                obj.AddComponent<NavMeshSourceTag>();
        }
    }
}

﻿using Assets.scripts.trees;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// Jeremy Gillespie-Cloutier (260688666)
// Class that contains information about the AI agent
public class Agent : MonoBehaviour
{

    public const float SIZE = 2f; // The size of the agent
    public const float SPEED = Level.P_HEIGHT / 3; // It takes 3 seconds for the agent to traverse the platform vertically
    [System.NonSerialized] public NavMeshAgent navAgent; // The nav mesh agent of the AI agent
    private Node root; // The root node of the agent's behavior tree
    private List<Node> tree = new List<Node>(); // ALl the nodes in the agent's behavior tree
    private Level level;
    private Game game;
    private bool treeCreated = false; // True if the agent's tree has been created
    private bool finished = false; // True if the agent has finished executing his behavior tree
    public int spawnIndex; // Index of the room the agent spawned in

    // Use this for initialization
    void Awake()
    {
        level = GameObject.Find("Level").GetComponent<Level>();
        game = GameObject.Find("Level").GetComponent<Game>();
        transform.localScale = new Vector3(1, 1, 1) * SIZE;
        navAgent = GetComponent<NavMeshAgent>();
        navAgent.speed = SPEED;
    }

    // Creates the bahavior tree for the agent that takes into consideration pedestrians
    public void buildTreeWithPedestrians()
    {
        List<int> visited = new List<int>(); // List of room visited so far
        int numRooms = level.agentSpawnPoints.Count;
        int current = spawnIndex;
        visited.Add(current);
        List<Node> sequence = new List<Node>(); // List of children of the sequencer node in the bahavior tree
        // Visit all rooms
        while (visited.Count < numRooms)
        {
            // First try going to left room if possible, otherwise in front, and if this is not possible then go to right room
            // This ensures the last room visited is always the rightmost room in a row
            int left = current - 2;
            int right = current + 2;
            int front = (current % 2 == 0) ? (current + 1) : (current - 1);
            int next = -1;
            if (left >= 0 && !visited.Contains(left))
                next = left;
            else if (!visited.Contains(front))
                next = front;
            else
                next = right;

            // Get information about the closest points between the current room and the next room
            MoveInfo info = new MoveInfo(current, next, level);
            // Move to the cosest point in current room, then wait for area to clear, then move to closest point in other room, then move to spawn point in other room
            // We do this to minimize the distance the agent travels in lanes, because we don't want him to encounter pedestrians
            sequence.Add(new WalkNode(info.start, this, game));
            sequence.Add(new WaitNode(info.start, info.stop, game, level));
            sequence.Add(new WalkNode(info.stop, this, game));
            sequence.Add(new WalkNode(level.agentSpawnPoints[next], this, game));
            // Set next rrom to current room and continue
            current = next;
            if (!visited.Contains(current))
                visited.Add(current);
        }
        // Once we reached the last room, we need to move to the green area
        int sign = current == 7 ? -1 : 1; // -1 if we are in the bottom row, 1 if top
        Vector3 lastRoom = level.agentSpawnPoints[current];
        // Start is the point in the last room that is closest to the green area, and exit is the point in the green area
        // that is the closest to start
        Vector3 start = new Vector3(lastRoom.x + (Level.ROOM_WIDTH - Agent.SIZE) / 2f, lastRoom.y, sign * (Level.P_HEIGHT + Agent.SIZE) / 2f);
        Vector3 exit = new Vector3((Level.P_WIDTH + Level.EDGE_WIDTH) / 2f, lastRoom.y, sign * (Level.P_HEIGHT - Agent.SIZE) / 2f);
        // Move to start, wait for area to clear, then move to exit.
        sequence.Add(new WalkNode(start, this, game));
        sequence.Add(new WaitNode(start, exit, game, level));
        sequence.Add(new WalkNode(exit, this, game));

        tree.AddRange(sequence);
        // The root node is a parallele node that contains the sequencer node as well as a node that checks for collisions
        // Either collision will return false when it detects a collision, in whic case the agent failed
        // Or the sequencer node will complete and return true, and the agent will suceed
        List<Node> parallelNodes = new List<Node>();
        parallelNodes.Add(new CollisionNode(this, game)); // Checks for collisions
        parallelNodes.Add(new SequencerNode(sequence)); // Sequencer ndoe that executes all agent movements
        tree.AddRange(parallelNodes);
        root = new ParallelNode(parallelNodes);
        tree.Add(root);
        root.run();
        treeCreated = true;
    }

    // Creates the behavior tree for the agent that does not take into consideration pedestrians
    public void buildTreeWithoutPedestrians()
    {
        List<Node> goals = new List<Node>();
        foreach (Vector3 point in level.agentSpawnPoints)
        {
            if (point != transform.position)
            {
                WalkNode node = new WalkNode(point, this, game);
                tree.Add(node);
                goals.Add(node);
            }
        }
        // The final destination (green area)
        WalkNode destination = new WalkNode(new Vector3((Level.P_WIDTH + Level.EDGE_WIDTH) / 2f, transform.position.y, 0), this, game);
        tree.Add(destination);
        goals.Add(destination);
        root = new SequencerNode(goals);
        tree.Add(root);
        root.run();
        treeCreated = true;
    }

    // udpate each node in the tree
    private void updateTree()
    {
        foreach (Node node in tree)
            if (node.isRunning())
                node.update(Time.deltaTime);
    }

    // Return the result of the root node
    public bool getResult()
    {
        return root.getResult();
    }

    // True if the agent has finished executing his behavior tree
    public bool isFinished()
    {
        return finished;
    }

    // Update is called once per frame
    void Update()
    {
        if (!finished && treeCreated && game.running) // If the agent's tree is created and not finished
        {
            updateTree(); // Update the nodes in the tree
            // If the root node is completed, we are done
            if (root.isCompleted())
                finished = true;
        }
    }

}

// Determines the closest points in 2 adjacent rooms
class MoveInfo
{
    public Vector3 start; // The closest point to room2 in room1
    public Vector3 stop;// The closest point to room1 in room2

    // Assumes room1 and room2 are adjacent (either same row or col)
    public MoveInfo(int room1, int room2, Level level)
    {
        Vector3 spawn1 = level.agentSpawnPoints[room1];
        Vector3 spawn2 = level.agentSpawnPoints[room2];
        bool sameCol = Mathf.Abs(spawn1.x - spawn2.x) < 0.5f;
        bool sameRow = (room1 % 2) == (room2 % 2);
        if (sameCol) // If the two rooms are in the same column
        {
            // Then the start and stop points are the 2 points that face each other just outside the lanes at the exit of these rooms
            if (spawn1.z < spawn2.z)
                start = new Vector3(spawn1.x, spawn1.y, -(Level.P_HEIGHT + Agent.SIZE) / 2f);
            else
                start = new Vector3(spawn1.x, spawn1.y, (Level.P_HEIGHT + Agent.SIZE) / 2f);

            stop = new Vector3(start.x, start.y, -start.z);
        }
        else if (sameRow) // If the two rooms are in the same row
        {
            // Then the start points are the left/right or right/left corners near the lane in front of the rows
            int rowSign = (spawn1.z < 0) ? -1 : 1;
            if (spawn1.x < spawn2.x)
            {
                start = new Vector3(spawn1.x + (Level.ROOM_WIDTH - Agent.SIZE) / 2f, spawn1.y, rowSign * (Level.P_HEIGHT + Agent.SIZE) / 2f);
                stop = start + new Vector3(Level.ROOM_DISTANCE - Level.ROOM_WIDTH + Agent.SIZE, 0, 0);
            }
            else
            {
                start = new Vector3(spawn1.x - (Level.ROOM_WIDTH - Agent.SIZE) / 2f, spawn1.y, rowSign * (Level.P_HEIGHT + Agent.SIZE) / 2f);
                stop = start - new Vector3(Level.ROOM_DISTANCE - Level.ROOM_WIDTH + Agent.SIZE, 0, 0);
            }
        }
    }
}
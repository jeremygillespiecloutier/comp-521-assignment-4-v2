﻿using UnityEngine;

namespace Assets.scripts.grammar
{
    // Jeremy Gillespie-Cloutier (260688666)
    // Action that makes the pedestrian look left and then right
    class LookAction : Action
    {
        float rotation = 0; // total rotation done by the pedestrian
        Quaternion initialRotation; // Contains the rotation of the pedestrian like it was at the beginning of the action
        public LookAction(Pedestrian pedestrian) : base(pedestrian) { }

        public override void begin()
        {
            // Store the pedestrian's initial rotation
            rotation = 0;
            initialRotation = pedestrian.transform.rotation;
            pedestrian.animator.SetBool("walking", false);
        }

        public override void enact(float deltaTime)
        {
            rotation += deltaTime * Pedestrian.LOOK_SPEED;
            rotation = Mathf.Clamp(rotation, 0, 360);
            // From middle to left (0 to 90 deg) and right to middle (270 to 360 deg) we go clockwise
            float amount = rotation;
            // Otherwise, from left to middle (90 to 180) and middle to right (180 to 270) we og counter clockwise
            if (rotation > 90 && rotation <= 270)
                amount = 180 - rotation;
            // Set the pedestrian rotation to the right amount
            pedestrian.transform.rotation = initialRotation;
            pedestrian.transform.Rotate(new Vector3(0, amount, 0));
            // We are done once the pedestrian has finished looking left and right
            if (rotation >= 360)
                completed = true;
        }

        public override void finish()
        {
            pedestrian.transform.rotation = initialRotation; // Reset the pedestrian's rotation
        }

        public override string toWord()
        {
            return "-look-";
        }
    }
}

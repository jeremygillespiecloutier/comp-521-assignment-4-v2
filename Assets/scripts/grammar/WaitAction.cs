﻿using System;

namespace Assets.scripts.grammar
{
    class WaitAction : Action
    {

        private float time, waited = 0;

        public WaitAction(Pedestrian pedestrian, float time) : base(pedestrian)
        {
            this.time = time;
        }

        public override void enact(float deltaTime)
        {
            waited += deltaTime;
            if (waited > time)
                completed = true;
        }

        public override string toWord()
        {
            return String.Format("-wait({0})-", time);
        }

        public override void begin()
        {
            pedestrian.animator.SetBool("walking", false);
        }

        public override void finish() { }
    }
}

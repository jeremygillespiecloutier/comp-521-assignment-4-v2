﻿using System.Collections.Generic;
using UnityEngine;

// Jeremy Gillespie-Cloutier (260688666)
// This class generates grammars procedurally for pedestrians
namespace Assets.scripts.grammar
{
    class Grammar
    {
        const float EPSILON = 0.1f;
        const float DEPTH = 4; // The recursion depth
        public List<Action> actions = new List<Action>(); // The list of actions (words) in this grammar
        private Pedestrian pedestrian; // pedestrian associated with this action
        public string sentence { get; private set; } // The grammar's sentence (concatenation of all words in the grammar)
        Action currentAction; // The current action being executed
        public bool completed { get; private set; } // Returns true if all actions i the grammar have been completed
        Level level;

        // Creates a grammar for the pedestrian that moves it from start to stop
        public Grammar(Pedestrian pedestrian)
        {
            this.pedestrian = pedestrian;
            level = GameObject.Find("Level").GetComponent<Level>();
            generate();
        }

        // Generates the pedestrian actions using grammar rules recursively
        void generate()
        {
            // The initial move is to move from the start area to the stop area
            Vector3 start = pedestrian.transform.position;
            Vector3 stop = start + new Vector3(Level.P_WIDTH + Level.EDGE_WIDTH, 0, 0);
            MoveAction move = new MoveAction(pedestrian, start, stop);
            // Use grammar rules to generate more actions from the inital action
            actions = applyRule(move, 1);
            currentAction = actions[0];
            actions.RemoveAt(0);
        }

        // Applies a grammar rule to a move, and then recurses on the different parts generated by the chosen rule
        List<Action> applyRule(MoveAction move, int level)
        {
            // If we reached recursion limit, then we don't further change the current move
            List<Action> actionList = new List<Action>(); // The resulting moves after applying a rule to current move and recursinsg
            if (level >= DEPTH)
            {
                actionList.Add(move);
                return actionList;
            }
            int newAction = Random.Range(0, 5); // Choose which gramar rule to apply
            List<Action> splitMove = new List<Action>(); // The resulting moves after applying the grammar rule

            if (newAction == 0) // Apply grammar rule to insert a wait
                splitMove = insertWait(move);
            else if (newAction == 1) // Apply grammar rule to insert a lane change
                splitMove = insertLaneChange(move);
            else if (newAction == 2) // If possible, apply a grammar rule to deviate
                splitMove = insertDeviation(move);
            else if (newAction == 3 && canBacktrack(move)) // If possible, apply a grammar rule to backtrack
                splitMove = insertBacktrack(move);
            else if (newAction == 4) // Apply a grammar rule to look around
                splitMove = insertLook(move);
            else // Otherwise just split the move in to without adding anything
                splitMove.AddRange(move.split(Random.Range(.4f, .6f)));

            // Moves can be either split in two or tree parts. If 3, we recurse one 1st and third, otherwise one 1st and second
            actionList.AddRange(applyRule((MoveAction)splitMove[0], level + 1));
            if (splitMove.Count == 3)
                actionList.Add(splitMove[1]);
            actionList.AddRange(applyRule((MoveAction)splitMove[splitMove.Count == 3 ? 2 : 1], level + 1));
            return actionList;
        }

        // Grammar rule that changes a move into move-look-move
        List<Action> insertLook(MoveAction move)
        {
            List<Action> newActions = new List<Action>();
            float location = Random.Range(.45f, .55f); // The location where look is inserted
            LookAction look = new LookAction(pedestrian); // The new action
            MoveAction[] split = move.split(location); // The split move
            // The resulting move is the first part of the split, then the look, the the second part
            newActions.Add(split[0]);
            newActions.Add(look);
            newActions.Add(split[1]);
            return newActions;
        }

        // Grammar rule that changes move into move-wait-move
        List<Action> insertWait(MoveAction move)
        {
            List<Action> newMoves = new List<Action>();
            float location = Random.Range(0.3f, 0.7f);// The location at which the wait occurs
            float time = Random.Range(0.5f, 2f); // The waiting duration
            // Split the move, then resturn first part of split followed by wait followed by second part
            MoveAction[] split = move.split(location);
            WaitAction wait = new WaitAction(pedestrian, time);
            newMoves.Add(split[0]);
            newMoves.Add(wait);
            newMoves.Add(split[1]);
            return newMoves;
        }

        float minBackLocation = 0.4f, maxBackLocation = 0.6f; // The min and max location a backtrack can be inserted
        float minBackLength = 2f, maxBackLength = 5f; // The min and max length of backtracks

        // A pedestrian can only backtrack if he is sufficiently far from the spawn point
        bool canBacktrack(MoveAction move)
        {
            return ((move.start + minBackLocation * move.diff).x - Level.START_X) >= maxBackLength;
        }

        // Grammar rule that changes move into move-backtrack-move
        List<Action> insertBacktrack(MoveAction move)
        {
            List<Action> newMoves = new List<Action>();
            // Choose where to insert the backtrack and how long it is
            float location = Random.Range(minBackLocation, maxBackLocation);
            float length = Random.Range(minBackLength, maxBackLength);
            MoveAction[] split = move.split(location);
            // Create the baktrack move, and modify the second part of the split so it begins at the end of the backtrack
            MoveAction backtrack = new MoveAction(pedestrian, split[0].stop, split[0].stop + new Vector3(-length, 0, 0));
            split[1].start = backtrack.stop;
            newMoves.Add(split[0]);
            newMoves.Add(backtrack);
            newMoves.Add(split[1]);
            return newMoves;
        }

        // Gramar rule that changes move into move-move, where the end of the first move and start of second move are at a deviated location
        List<Action> insertDeviation(MoveAction move)
        {
            List<Action> newMoves = new List<Action>();
            // Get current lane and spawn point of current lane
            int lane = getLane(move.start);
            Vector3 laneSpawn = level.pedestrianSpawnPoints[lane];
            float location = Random.Range(0.38f, 0.62f); // where to insert the deviation
            MoveAction[] split = move.split(location);
            Vector3 deviated = new Vector3(split[0].stop.x, split[0].stop.y, laneSpawn.z + Level.LANE_HEIGHT / 4f); // Deviated point if we deviate up
            if (move.start.z > laneSpawn.z)// Deviate down, since we are in upper part of lane
                deviated = new Vector3(deviated.x, deviated.y, laneSpawn.z - Level.LANE_HEIGHT / 4f); // Deviated point if we deviate down
            split[0].stop = deviated;
            split[1].start = deviated;
            newMoves.Add(split[0]);
            newMoves.Add(split[1]);
            return newMoves;
        }

        // Grammar rule that changes move into move-change lane-move
        List<Action> insertLaneChange(MoveAction move)
        {
            float location = Random.Range(0.3f, 0.7f); // The point at which the initial move gets split
            List<Action> newMoves = new List<Action>();
            // split the move
            MoveAction[] split = move.split(location);
            MoveAction first = split[0];
            MoveAction second = split[1];
            // Get the pedestrian lane. If he is in the bottom lane (0), he can only go up.
            // If he is in the top lane, he can only go down.
            // Otherwise he can go up or down
            int lane = getLane(first.stop);
            List<int> laneChoices = new List<int>();
            if (lane == 0)
                laneChoices.Add(1);
            else if (lane == level.pedestrianSpawnPoints.Count - 1)
                laneChoices.Add(lane - 1);
            else
            {
                laneChoices.Add(lane + 1);
                laneChoices.Add(lane - 1);
            }
            // Randomly choose the pedestrian's new lane amongst the valid chocies
            int newLane = laneChoices[Random.Range(0, laneChoices.Count)];
            Vector3 chosen = level.pedestrianSpawnPoints[newLane]; // The spawn point in that lane
            // change the second part of the split move so that the z value of the start and stop points corresponds to the z value of the new lane
            second.start = new Vector3(second.start.x, second.start.y, chosen.z);
            second.stop = new Vector3(second.stop.x, second.stop.y, chosen.z);
            // Create a move from the stop point of the first part of the split to the start point of the second part (i.e. lane change)
            MoveAction change = new MoveAction(pedestrian, first.stop, second.start);
            // Then return the first part, followed by the lane change, followed by the second part (which is now in the new lane)
            newMoves.Add(first);
            newMoves.Add(change);
            newMoves.Add(second);
            return newMoves;
        }

        // Determines the lane closest to the pedestrian
        int getLane(Vector3 position)
        {
            int lane = -1;
            float closest = float.MaxValue;
            for (int i = 0; i < level.pedestrianSpawnPoints.Count; i++)
            {
                Vector3 spawn = level.pedestrianSpawnPoints[i];
                float distance = Mathf.Abs(spawn.z - position.z);
                if (distance < closest)
                {
                    closest = distance;
                    lane = i;
                }
            }
            return lane;
        }

        bool first = false; // flag indicating if the first action in the grammar has begun

        // Called at each frame by the pedestrian
        public void update(float deltaTime)
        {
            if (!first) // Start the first action
            {
                first = true;
                currentAction.begin();
            }
            if (!completed)
            {
                // execute the current action
                currentAction.enact(Time.deltaTime);
                if (currentAction.completed)
                {
                    // When current action is done, finish it and remove it from the list of actions
                    currentAction.finish();
                    actions.RemoveAt(0);
                    if (actions.Count == 0) // If there are noi actions remaining, then the grammar execution is over
                    {
                        currentAction = null;
                        completed = true;
                    }
                    else // Otherwise fetch and begin the next action
                    {
                        currentAction = actions[0];
                        currentAction.begin();
                    }
                }
            }
        }
    }
}

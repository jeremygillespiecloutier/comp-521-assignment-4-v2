﻿using System;
using UnityEngine;

namespace Assets.scripts.grammar
{
    // Jeremy Gillespie-Cloutier (260688666)
    // Pedestrian action to move from a start point to a destination
    class MoveAction : Action
    {

        public Vector3 start, stop; // The start and stop points of the movement
        public Vector3 diff; // The movement vector (stop-start)
        float distance = 0, totalDistance; // Distance travelled so far and total distance

        // Action to move the pedestrian from start to stop
        public MoveAction(Pedestrian pedestrian, Vector3 start, Vector3 stop) : base(pedestrian)
        {
            this.start = start;
            this.stop = stop;
            diff = stop - start;
            totalDistance = diff.magnitude;
        }

        public MoveAction[] split(float location)
        {
            // Split the current move into twpo moves at the given location (0 to 1, 0 being the start and 1 the stop)
            MoveAction[] newMoves = new MoveAction[2];
            MoveAction first = new MoveAction(pedestrian, start, start + location * diff);
            MoveAction second = new MoveAction(pedestrian, first.stop, stop);
            newMoves[0] = first;
            newMoves[1] = second;
            return newMoves;
        }

        public override void begin()
        {
            // Set the pedestrian destination and start the walk animation
            pedestrian.animator.SetBool("walking", true);
            pedestrian.navAgent.avoidancePriority = pedestrian.priority;
            pedestrian.navAgent.isStopped = false;
            pedestrian.navAgent.destination = stop;
        }

        public override void finish()
        {
            // Stop the pedestrian animation
            pedestrian.animator.SetBool("walking", false);
            pedestrian.navAgent.isStopped = true;
        }

        public override void enact(float deltaTime)
        {
            distance += deltaTime * Pedestrian.SPEED;
            distance = Mathf.Clamp(distance, 0, totalDistance);

            // Stop once the pedestrian reaches the stop point (within some range, as navMeshAgent never stops exactly at right pos)
            if ((pedestrian.transform.position - stop).magnitude < 0.5f)
                completed = true;
        }

        public override string toWord()
        {
            return String.Format("-move(({0:F3}, {1:F3}), ({2:F3}, {3:F3}))-", start.x, start.y, stop.x, stop.y);
        }
    }
}

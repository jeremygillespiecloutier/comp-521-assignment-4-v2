﻿
// Jeremy Gillespie-Cloutier
// Base class for actions in the grammar (i.e. words)
namespace Assets.scripts.grammar
{
    abstract class Action
    {

        protected Pedestrian pedestrian; // The pedestrian associated with this action
        public bool completed { get; protected set; }

        public Action(Pedestrian pedestrian)
        {
            this.pedestrian = pedestrian;
        }

        // Execute the action
        public abstract void enact(float deltaTime);

        // Return this action as a word of the grammar
        public abstract string toWord();

        // Called when the action first begins
        public abstract void begin();

        // Called when the action is finished
        public abstract void finish();
    }

}

﻿using System.Collections.Generic;

namespace Assets.scripts.trees
{
    // Jeremy Gillespie-Cloutier (260688666)
    // Composite node that executes all its children at the same time and returns as soon as one children is done
    class ParallelNode : CompositeNode
    {
        public ParallelNode(List<Node> children) : base(children) { }

        public override void update(float deltaTime)
        {
            foreach (Node child in children)
            {
                child.run();
                if (child.isCompleted())
                {
                    finish(child.getResult());
                    return;
                }
            }
        }
    }
}

﻿
namespace Assets.scripts.trees
{
    // Jeremy Gillespie-CLoutier (260688666)
    // This node returns false if the agent has collided with a pedestrian
    class CollisionNode : LeafNode
    {
        private Agent agent;
        private Game game;

        public CollisionNode(Agent agent, Game game)
        {
            this.agent = agent;
            this.game = game;
        }

        public override void update(float deltaTime)
        {
            // No need to check for collisions if we are in a room
            if (agent.transform.position.z > Level.P_HEIGHT / 2f || agent.transform.position.z < -Level.P_HEIGHT / 2f)
                return;
            // If we are within a lane width radius of a pedestrian, return false.
            foreach (Pedestrian pedestrian in game.pedestrians)
            {
                if ((pedestrian.transform.position - agent.transform.position).magnitude <= Level.LANE_HEIGHT)
                {
                    finish(false);
                    return;
                }
            }
        }
    }
}

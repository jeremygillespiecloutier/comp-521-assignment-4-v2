﻿using UnityEngine;

namespace Assets.scripts.trees
{
    // Jeremy Gillespie-Cloutier (260688666)
    // Waits for the path to between 2 rooms to clear and returns true once it does
    class WaitNode : LeafNode
    {
        private Game game;
        private Level level;
        private float minX, maxX, minY, maxY;

        public WaitNode(Vector3 point1, Vector3 point2, Game game, Level level)
        {
            this.game = game;
            this.level = level;
            // Pedestrians move 1.5 faster than agents, so check at this distance left.
            // However, check less far right, as it is less likely that pedestrians backtrack for long distances
            minX = Mathf.Min(point1.x, point2.x) - 1.5f * Level.LANE_HEIGHT;
            maxX = Mathf.Max(point1.x, point2.x) + Level.LANE_HEIGHT;
            minY = Mathf.Min(point1.y, point2.y) - 1.5f * Level.LANE_HEIGHT;
            maxY = Mathf.Max(point1.y, point2.y) + Level.LANE_HEIGHT;
        }

        public override void update(float deltaTime)
        {
            // Check if the path is clear
            bool clear = true;
            foreach (Pedestrian pedestrian in game.pedestrians)
            {
                Vector3 pos = pedestrian.transform.position;
                if (pos.x >= minX && pos.x <= maxX && pos.y >= minY && pos.y <= maxY)
                {
                    clear = false;
                    break;
                }
            }
            if (clear)
                finish(true);
        }
    }
}

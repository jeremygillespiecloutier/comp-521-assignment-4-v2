﻿using UnityEngine;

namespace Assets.scripts.trees
{
    // Jeremy Gillespie-Cloutier (260688666)
    // Leaf node representing a walk action for the agent
    class WalkNode : LeafNode
    {
        private Vector3 destination; // The agent's destination
        private Agent agent; // The agent 
        private Game game; // Game object
        private bool destinationSet = false; // True if the agent's destination has been set

        public WalkNode(Vector3 destination, Agent agent, Game game)
        {
            this.destination = destination;
            this.agent = agent;
            this.game = game;
        }

        public override void prepare()
        {
            destinationSet = false;
        }

        public override void update(float deltaTime)
        {
            if (!destinationSet) // Set the agent's destination. Needs to be here otherwise agent is not on navmesh
            {
                agent.navAgent.destination = destination;
                destinationSet = true;
            }
            // If we are near goal, return true
            if ((agent.transform.position - destination).magnitude < 0.25f)
            {
                finish(true);
                return;
            }
        }

    }
}

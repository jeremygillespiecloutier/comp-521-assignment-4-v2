﻿using UnityEngine;

namespace Assets.scripts.trees
{
    // Jeremy Gillespie-CLoutier (260688666)
    // This node returns true if the agent has reached the given destination
    class DestinationNode : LeafNode
    {
        private Agent agent;
        private Vector3 destination;

        public DestinationNode(Agent agent, Vector3 destination)
        {
            this.agent = agent;
            this.destination = destination;
        }

        public override void update(float deltaTime)
        {
            if ((agent.transform.position - destination).magnitude < 0.5f)
                finish(true);
            else
                finish(false);
        }
    }
}

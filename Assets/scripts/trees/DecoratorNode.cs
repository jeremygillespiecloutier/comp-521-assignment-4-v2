﻿
namespace Assets.scripts.trees
{
    // Jeremy Gillespie-Clouteir (260688666)
    // Base class for decorator nodes
    abstract class DecoratorNode : Node
    {
        public Node child { get; private set; } // The child node this node decorates

        // Create a decorator node that decorates the given child
        public DecoratorNode(Node child)
        {
            this.child = child;
        }

        // Reset the child this node decorates as well as current node
        public override void reset()
        {
            base.reset();
            child.reset();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.scripts.trees
{
    class PositiveRepeaterNode : DecoratorNode
    {
        public PositiveRepeaterNode(Node child) : base(child) { }

        public override void update(float deltaTime)
        {
            child.run();
            if (child.isCompleted())
            {
                if (child.getResult())
                    finish(true);
                else
                {
                    child.reset();
                }
            }
        }
    }
}

﻿
namespace Assets.scripts.trees
{
    // Jeremy Gillespie-Cloutier (260688666)
    // Inverter is a decorator node that negates the result of its child
    class InverterNode : DecoratorNode
    {
        public InverterNode(Node child) : base(child) { }

        public override void update(float deltaTime)
        {
            // Run the child node
            child.run();
            // When de child node has finished running, set the result to the opposite of the child's result
            if (child.isCompleted())
            {
                bool result = child.getResult();
                finish(!result);
            }
        }

    }
}

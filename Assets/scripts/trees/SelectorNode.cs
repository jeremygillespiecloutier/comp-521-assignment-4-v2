﻿using System.Collections.Generic;

namespace Assets.scripts.trees
{
    // Jeremy Gillespie-Cloutier (260688666)
    // Selector is a composite node that returns true as soon as one child node is successful, and  false otherwise
    class SelectorNode : CompositeNode
    {
        public SelectorNode(List<Node> children) : base(children) { }

        public override void update(float deltaTime)
        {
            // Run the children in order
            foreach (Node node in children)
            {
                node.run();
                // if current node has not finished running, we wait for it
                if (!node.isCompleted())
                    return;
                // Otherwise get result. If true, we return true
                bool result = node.getResult();
                if (result)
                {
                    finish(true);
                    return;
                }
            }
            finish(false); // otherwise the children all returned false, so return false
        }
    }
}

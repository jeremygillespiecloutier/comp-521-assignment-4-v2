﻿using System.Collections.Generic;

namespace Assets.scripts.trees
{
    // Jeremy Gillespie-Cloutier (260688666)
    // Base class for composite nodes
    abstract class CompositeNode : Node
    {
        public List<Node> children { get; private set; } // The children of the node

        // Creates a composite node with the given children
        public CompositeNode(List<Node> children)
        {
            this.children = children;
        }

        // Reset the current node and it's children
        public override void reset()
        {
            base.reset();
            foreach (Node node in children)
                node.reset();
        }

    }
}

﻿
namespace Assets.scripts.trees
{
    // Jeremy Gillespie-Cloutier (260688666)
    // Base class for behavior tree nodes
    abstract class Node
    {
        private bool running = false, completed = false; // Flags indicating if the node is currently running and completed
        private bool result; // The result of this node's execution (success or failure)

        // Start executing the node
        public void run()
        {
            if (!running && !completed) // If the node switches from not running to running, call prepare()
                prepare();
            running = true;
        }

        // Determine if the current node is running
        public bool isRunning()
        {
            return running;
        }

        // Determine if the current node has finished running
        public bool isCompleted()
        {
            return completed;
        }

        // Indicate that the current node has finished running with the given result
        public void finish(bool result)
        {
            if (running)
            {
                completed = true;
                running = false;
                this.result = result;
            }
        }

        // Get the result of the node's execution
        public bool getResult()
        {
            return this.result;
        }

        // Update is called at each fram on nodes that are running
        public abstract void update(float deltaTime);

        // Reset the node so that it can be executed again
        public virtual void reset()
        {
            running = false;
            completed = false;
        }

        // Called just before a node starts running
        public virtual void prepare() { }

    }
}

﻿using System.Collections.Generic;

namespace Assets.scripts.trees
{
    // Jeremy Gillespie-Cloutier (260688666)
    // Sequencer is a composite node that returns true if all the children return true
    class SequencerNode : CompositeNode
    {
        public SequencerNode(List<Node> children) : base(children) { }

        public override void update(float deltaTime)
        {
            foreach (Node node in children)
            {
                // Run the current children, and if it is not completed, wait for it to finish
                node.run();
                if (!node.isCompleted())
                    return;
                // Get the result, and if it is false, return false
                bool childResult = node.getResult();
                if (!childResult)
                {
                    finish(false);
                    return;
                }
            }
            finish(true); // Otherwise all children have returned true, so return true
        }
    }
}

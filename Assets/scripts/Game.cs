﻿using System.Collections.Generic;
using UnityEngine;

// Jeremy Gillespie-Cloutier (260688666)
// Class that manages the game
public class Game : MonoBehaviour
{

    public const float EPSILON = 0.001f;
    const float CAM_SPEED = 35f; // The speed at which the camera moves
    const float SCROLL = 5f; // The mouse scroll multiplier for zooming
    const float CAM_MIN = 15f; // The min distance of the camera to the platform
    const float CAM_MAX = 50f; // The max distance of the camera
    const float CAM_BOUND_X = (Level.P_WIDTH + 2 * Level.EDGE_WIDTH) / 2f; // The max distance the camera can move from the center horizontally
    const float CAM_BOUND_Y = (Level.P_HEIGHT + 2 * Level.ROOM_HEIGHT) / 2f; // The max distance the camera can move from the center vertically
    float camDistance = 40; // The current distance of the camera
    Agent agent;
    Level level;
    Menu menu;
    [System.NonSerialized] public List<Pedestrian> pedestrians = new List<Pedestrian>(); // List of pedestrians currently active
    public float numPedestrians = 4; // Number of pedestrians active at any time
    public bool running = true; // flag indicating if the game is running (false when agent either reaches destination or fails)
    [System.NonSerialized] public bool followAgent = false; // If true, the camera will follow the agent
    List<int> priorities = new List<int>(); // Pool of priorities to ensure that each pedestrian has a different navMeshAgent priority (so they don't get stuck)

    // Use this for initialization
    void Start()
    {
        agent = GameObject.Find("Agent").GetComponent<Agent>();
        level = GameObject.Find("Level").GetComponent<Level>();
        menu = GameObject.Find("Canvas").GetComponent<Menu>();
        // Allocate priorities for the pedestrians
        for (int i = 0; i < numPedestrians; i++)
            priorities.Add(i + 1);
        spawnAgent();
        positionCamera();
    }

    void spawnAgent()
    {
        // Randomly choose where to spawn the agent
        int index = Random.Range(0, level.agentSpawnPoints.Count);
        Vector3 spawn = level.agentSpawnPoints[index];
        agent.transform.position = spawn;
        agent.spawnIndex = index;
        // Build his behavior tree
        agent.buildTreeWithPedestrians();
    }

    void positionCamera()
    {
        // Initial placement of the camera is centered above the agent
        Camera.main.transform.position = new Vector3(agent.transform.position.x, camDistance, agent.transform.position.z);
        Camera.main.transform.up = new Vector3(0, 0, 1);
        Camera.main.transform.forward = new Vector3(0, -1, 0);
        Camera.main.transform.LookAt(agent.transform.position);
    }

    void Update()
    {
        if (!agent.isFinished())
        {
            updateCamera();
            spawnPedestrians();
        }
        else if (running) //If we just finished, show the end game menu
        {
            running = false;
            foreach (Pedestrian p in pedestrians)
            {
                p.animator.SetBool("walking", false);
                p.navAgent.isStopped = true;
            }
            menu.showResult(agent.getResult());
        }
    }

    void spawnPedestrians()
    {
        // Remove all pedestrians that have completed all their actions
        for (int i = pedestrians.Count - 1; i >= 0; i--)
            if (pedestrians[i].completed)
            {
                priorities.Add(pedestrians[i].priority); // Return pedestrian's priority to the pool
                // Remove the pedestrian once it is completed
                Destroy(pedestrians[i].gameObject);
                pedestrians.RemoveAt(i);
            }
        // Spawn pedestrians (only one pedestrian will be spawned each frame, but this is indistinguishable)
        if (pedestrians.Count < numPedestrians)
        {
            List<Vector3> spawnChoices = new List<Vector3>();
            // Gather the possible spawn positions
            foreach (Vector3 spawn in level.pedestrianSpawnPoints)
            {
                // Can't spawn a pedestrian at a location too close to another one
                bool canSpawn = true;
                foreach (Pedestrian p in pedestrians)
                {
                    float distance = (p.transform.position - spawn).magnitude;
                    if (distance < 5f)
                    {
                        canSpawn = false;
                        break;
                    }
                }
                if (canSpawn)
                    spawnChoices.Add(spawn);
            }
            if (spawnChoices.Count > 0)
            {
                // If there are some valid spawn locations available, then choose one and put a pedestrian there
                int index = Random.Range(0, spawnChoices.Count);
                GameObject pedestrianObject = (GameObject)Instantiate(Resources.Load("objects/Pedestrian"));
                pedestrianObject.transform.position = spawnChoices[index] + new Vector3(0, 0.5f, 0);
                Pedestrian pedestrian = pedestrianObject.GetComponent<Pedestrian>();
                pedestrians.Add(pedestrian);
                // Remove a priority from the pool and assign it to the pedestrian
                pedestrian.priority = priorities[0];
                priorities.RemoveAt(0);
            }
        }
    }

    void updateCamera()
    {
        // Calculate the camera movement vector (W-A-S-D to move)
        Vector3 camMove = Vector3.zero;
        if (Input.GetKey(KeyCode.A))
            camMove += new Vector3(-1, 0, 0);
        else if (Input.GetKey(KeyCode.D))
            camMove += new Vector3(1, 0, 0);
        if (Input.GetKey(KeyCode.W))
            camMove += new Vector3(0, 0, 1);
        if (Input.GetKey(KeyCode.S))
            camMove += new Vector3(0, 0, -1);
        if (camMove.sqrMagnitude > EPSILON)
        {
            camMove.Normalize();
            camMove *= Time.deltaTime * CAM_SPEED;
        }

        // Calculate how much to change the camera distance if the mouse was scrolled
        Vector3 camPos = Camera.main.transform.position;
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        camDistance -= scroll * SCROLL;
        camDistance = Mathf.Clamp(camDistance, CAM_MIN, CAM_MAX);
        // If followAgent flag is set, camera is centered on agent, otherwise update camera position with move vector.
        if (followAgent)
            camPos = new Vector3(agent.transform.position.x, camDistance, agent.transform.position.z);
        else
            camPos = new Vector3(camPos.x, camDistance, camPos.z) + camMove;
        // Ensure camera remains within bounds of the level
        camPos = new Vector3(Mathf.Clamp(camPos.x, -CAM_BOUND_X, CAM_BOUND_X), camPos.y, Mathf.Clamp(camPos.z, -CAM_BOUND_Y, CAM_BOUND_Y));
        Camera.main.transform.position = camPos;
    }

}
